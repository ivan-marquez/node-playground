import { Greeter } from './greeter/greeter';
import { CarShow } from './events/callbacks';
import { FileManager } from './files/file-manager';
import { Censorify } from './censorify/censorify';
import { SetInterval } from './timers/set-interval';
import { NextTick } from './timers/next-tick';
import { Account, displayBalance, checkOverdraw, checkGoal } from './events/event-emitter';
import { setMaxListeners } from "cluster";

/////////////////////////////////////////////////////

// let greeter = new Greeter('Hello world!');
// console.log(greeter.greet());

/////////////////////////////////////////////////////

// let censor = new Censorify();
// console.log(censor.getCensoredWords());
// console.error(censor.censor('Some very sad, bad and mad text.'));
//
// censor.addCensoredWord('gloomy');
// console.log(censor.getCensoredWords());
// console.error(censor.censor('A very gloomy day.'));

/////////////////////////////////////////////////////

// let interval = new SetInterval();
// interval.initialize();

/////////////////////////////////////////////////////

// let tick = new NextTick();
// tick.initialize();

/////////////////////////////////////////////////////

// let account = new Account();
//
// account.on(Account.balanceChanged, () => displayBalance(account.balance));
// account.on(Account.balanceChanged, () => checkOverdraw(account.balance));
// account.on(Account.balanceChanged, () => checkGoal(account, 1000));
//
// account.deposit(220);
// account.deposit(320);
// account.deposit(600);
// account.deposit(1200);

/////////////////////////////////////////////////////

// let carShow = new CarShow();
//
// function logCar(make: string) {
//   console.log(make);
// }
//
// function logColorCar(make: string, color: string) {
//   console.log(`Saw a ${color} ${make}`);
// }
//
// carShow.on(CarShow.sawCar, logCar);
// carShow.on(CarShow.sawCar, function (make: string){
//   let colors = ['red', 'blue', 'black'];
//   let color = colors[Math.floor(Math.random() * 3)];
//
//   logColorCar(make, color);
// });
//
// carShow.seeCar('Ferrari');
// carShow.seeCar('Prosche');
// carShow.seeCar('Bugatti');
// carShow.seeCar('Lamborghini');
// carShow.seeCar('Aston Martin');

/////////////////////////////////////////////////////

let fileManager = new FileManager('C:\\Users\\jm5256\\Desktop\\Temp');
fileManager.readAllFiles();
