import { readdir } from 'fs';

export class FileManager {

  private path: string;

  constructor(path: string) {
    this.path = path;
  }

  readAllFiles() {
    readdir(this.path, (err, items) => {
      console.log(items);
    });
  }
}
