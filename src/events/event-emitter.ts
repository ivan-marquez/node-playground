import { EventEmitter } from 'events';

export class Account extends EventEmitter {

  balance: number;

  constructor() {
    super();
    this.balance = 0;
  }

  static get balanceChanged(): string {
    return 'balanceChanged';
  }

  deposit(amount: number) {
    this.balance += amount;
    this.emit(Account.balanceChanged);
  }

  withdraw(amount: number) {
    this.balance -= amount;
    this.emit(Account.balanceChanged);
  }

}

export function displayBalance(amount: number): void {
  console.log(`Account balance: ${amount}`);
}

export function checkOverdraw(balance: number): void {
  if (this.balance < 0) {
    console.log(`Account overdrawn! Balance: ${balance}`);
  }
}

export function checkGoal(acc: Account, goal: number): void {
  if (acc.balance > goal) {
    console.log('Goal achieved!');
  }
}
