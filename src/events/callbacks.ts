import { EventEmitter } from 'events';

export class CarShow extends EventEmitter {

  constructor() {
    super();
  }

  static get sawCar(): string {
    return 'sawCar';
  }

  seeCar(make: string) {
    this.emit('sawCar', make);
  }
}
