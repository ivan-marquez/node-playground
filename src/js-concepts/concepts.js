(function () { //IIFE - it keeps the global namespace from being poluted, prevents var hoisting and it can be used to create private namespaces.

  this.$Dev = this.$Dev || {}; // use it if it's already created. Otherwise, create a new one.
  var ns = this.$Dev;

  var ex = 'closure remembers me';

  ns.publicFunction = function (test) { // Closure example - a closure is created when you expose an inner function
    console.log(test, ex); // closures have access to all the args and vars from the outer function.
    console.log(typeof 10); // Is an operator that indicates the data type of the operand. In this case, number.

    if (true) {
      var hoist = 'variable hoisting'; //
    }

    console.log(hoist);
  };

  ns.Person = function (name, age, gender) {
    this.name = name; // `this` is pointing to the object's prototype.
    this.age = age;
    ns.Person.prototype.gender = gender; // same as this.gender. You use the prototype property to attach properties and
    // methods to make them available to instances of an object.
  };

  ns.Developer = function(name) {
    ns.Person.call(this, name, '29', 'male'); // we use the call() method to modify the this object. So, the call to the
    // parent's constructor creates a name, age, and gender properties in the Developer object.
  };

  ns.Developer.prototype = Object.create(ns.Person.prototype); // new Person();
  ns.Developer.prototype.constructor = ns.Developer;

}());

$Dev.publicFunction('testing stuff');

var me = new $Dev.Person('Ivan', 29, 'male', '// creating an object using new');
me.email = 'ivan@test.com';

console.log(me.name, me.age, me.gender, me.email, "// instance property, it's only available to this instance.");

var you  = new $Dev.Person('John', 25, 'male');
console.log(you.name, you.age, you.gender, you.email, '// email is not avaliable here.');

var newMe = new $Dev.Developer('Ivan Marquez');
console.log(newMe.name, newMe.age, newMe.gender, '// properties inherited from parent object');

