/**
 * Performing Periodic Work with Intervals
 * Interval timers are used to perform work on a regular delayed interval. When the delay time expires, the callback
 * function is executed and is then rescheduled for the delay interval again. You should use intervals for work that
 * needs to be performed on a regular basis.
 */

export class SetInterval {

  private x: number = 0;
  private y: number = 0;
  private z: number = 0;

  displayValues(): void {
    console.log(`X=${this.x}; Y=${this.y}; Z=${this.z}`);
  }

  updateX(): void {
    this.x += 1;
  }

  updateY(): void {
    this.y += 1;
  }

  updateZ(): void {
    this.z += 1;
    this.displayValues();
  }

  initialize(): void {
    setInterval(() => this.updateX(), 500);
    setInterval(() => this.updateY(), 1000);
    setInterval(() => this.updateZ(), 2000);
  }
}
