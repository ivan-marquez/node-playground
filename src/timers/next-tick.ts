import { stat } from 'fs';

export class NextTick {

  initialize() {
    stat(`${__dirname}/set-interval.js`, (err, stats) => {
      if (stats) {
        console.log('file exists.');
      }
    });

    setImmediate(() => console.log('Immediate Timer 1 executed.'));
    setImmediate(() => console.log('Immediate Timer 2 executed.'));

    process.nextTick(() => console.log('Next Tick 1 executed.'));
    process.nextTick(() => console.log('Next Tick 2 executed.'));
  }
}
