export class Censorify {

  private censoredWords: string[];
  private customCensoredWords: string[];

  constructor() {
    this.censoredWords = ['sad', 'bad', 'mad'];
    this.customCensoredWords = [];
  }

  censor(inStr: string): string {
    this.censoredWords.forEach(str => {
      inStr = inStr.replace(str, '****');
    });

    this.customCensoredWords.forEach(str => {
      inStr = inStr.replace(str, '****');
    });

    return inStr;
  }

  addCensoredWord(word: string): void {
    this.customCensoredWords.push(word);
  }

  getCensoredWords(): string[] {
    return this.censoredWords.concat(this.customCensoredWords);
  }
}
