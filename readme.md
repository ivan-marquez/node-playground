# Node.js


### So what is Node.js?

Node.js is a development environment that is based on Google’s V8 JavaScript engine. You write Node.js code in
JavaScript, and then V8 compiles it into machine code to be executed. You can write most (or maybe even all) of 
your server-side code in Node.js, including the web server and the server-side scripts and any supporting web 
application functionality.

###  Benefits

One of the most powerful features of the Node.js framework is the ability to easily extend it with additional Node
Packaged Modules (NPMs), using the Node Package Manager (NPM).

### What Are Node Packaged Modules?

A Node Packaged Module is a packaged library that can easily be shared, reused, and installed in different projects.
There are many different modules available for a variety of purposes. For example, the Mongoose module provides an
ODM for MongoDB, Express extends Node’s HTTP capabilities, and so on.

Node.js modules are created by various third-party organizations to provide important features that Node.js lacks out
of the box.

### Node Package Manager

The Node Package Manager you have already seen is a **command-line utility**. It allows you to find, install, remove,
publish, and do a lot of other things related to Node Packaged Modules. The Node Package Manager provides a link
between the Node Package Registry and your development environment.

### Using package.json

All Node modules must include a `package.json` file in their root directory. `package.json` is a simple JSON text file
that defines a module, including dependencies. The package.json file can contain a number of different directives to
tell the Node Package Manager how to handle the module.

### Understanding the Node.js Event Model

Node.js applications run in a single-threaded event-driven model. Although Node.js implements a thread pool in the
background to do work, the application itself doesn’t have any concept of multiple threads.


![multi-thread](assets/img/multi-thread-model.png)

In the traditional threaded web model, a request comes to a webserver and is assigned to an available thread. The
handling of work for that request continues on that thread until the request is complete and a response is sent.

![single-thread](assets/img/single-thread-model.png)

Instead of executing all the work for each request on individual threads, Node.js adds work to an event queue and then 
has a single thread running an event loop pick it up. The event loop grabs the top item in the event queue, executes
it, and then grabs the next item. When executing code that is longer lived or has blocking I/O, instead of calling the 
function directly, it adds the function to the event queue along with a callback that will be executed after the
function completes. When all events on the Node.js event queue have been executed, the Node.js application terminates.

### Blocking I/O in Node.js

![single-thread](assets/img/node-blocking-io.png)

Node.js uses event callbacks to avoid having to wait for blocking I/O. Therefore, any requests that perform blocking I/O
are performed on a different thread in the background. Node.js implements a thread pool in the background. When an event
that blocks I/O is retrieved from the event queue, Node.js retrieves a thread from the thread pool and executes the
function there instead of on the main event loop thread. This prevents the blocking I/O from holding up the rest of the
events in the event queue.

The function that is executed on the blocking thread can still add events back to the event queue to be processed. For
example, a database query call is typically passed a callback function that parses the results and may schedule
additional work on the event queue before sending a response.

**Note**: To leverage the scalability and performance of the event model, make sure you break up work into chunks that 
can be performed as a series of callbacks.     
